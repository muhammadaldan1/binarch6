
const express = require('express')
const { Sequelize, DataTypes } = require('sequelize')
const authenticate = require ("./modules/authenticate")
const models = require('./models')
const app = express()
const port = 3000



app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use("/img",express.static('assets'))
app.use(express.static('public'))
app.set('view engine', 'ejs') 
app.use(authenticate)


app.post("/members", async (req, res) => {
  await models.Member.create(req.body);
  res.send("New member already inputed")
  res.redirect("/");
});
///Read
app.get("/members", async (req, res) => {
  const members = await models.Member.findAll()
  res.send(members);
});




app.post("/books", async (req, res) => {
  await models.Book.create(req.body);
  res.send("New book already inputed")
  res.redirect("/");
});


app.get("/books", async (req, res) => {
  const books = await models.Book.findAll()
  res.send(books);
});





app.post("/activities", async (req, res) => {
  await models.Activity.create(req.body)
  res.redirect("/activities");
});

app.get("/activities", async (req, res) => {
  const activities = await models.Activity.findAll({
    include: [models.Member, models.Book],
    
  });
  res.render("pages/activity", { activities});
});

app.post("/edit-activities", async (req, res) => {
  if (req.body._method) {
    await models.Activity.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await models.Activity.create(req.body);
  }
  res.redirect("/activities");
});

app.post("/delete-activities/:id", async (req, res) => {
  const id =  req.params.id  
  await models.Activity.destroy({
    where:{
      id
    }
  })
  res.redirect("/activities")
});



app.get("/", (req, res) => {
  res.redirect("/login");
});

app.get("/add-activities", async (req, res) => {
  const members = await models.Member.findAll({});
  res.render("pages/register", { members });
});

app.get("/edit-activities/:id", async (req, res) => {
  const activity = await models.Activity.findOne({
    where: {
      id: Number(req.params.id),
    },
  });

  res.render("pages/edit", { activity });
});





app.use((req,res) => {
  res.status(404).render("pages/404")
})

app.use((err,req,res,next) =>{
  res.status(500).render("pages/500",{error: err.message})
  })



app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});